# Hello Wave

The goal of this is to graphically and acoustically illustrate the
transformation of a sine wave to a square wave, a triangle wave and a
sawtooth wave.

In addition, it will allow the user to adjust the frequency both
continuously, and in discrete steps of a western chromatic scale.

(Goal.. Fantasy... You say poe-tay-toe, I say poe-tah-toe.)

----

## License

Copyleft 2018 Kevin Cole (a.k.a. "The Ubuntourist")

This work? play? is licensed under the [GNU General Public License
(GPL) Version 3](LICENSE.md).

----

## Prerequisites

At the moment, this is being developed on an Ubuntu 16.04 LTS system.

The big two requirements are Python3 and Qt 5. This of course means
PyQt5 as well.

NumPy (and eventually SciPi) Python modules, and the SoundDevice
module round things out.

Just:

    ./install.sh

which will install all of the above as Debian packages, except for
SoundDevice, which comes as a `pip install`.

----

