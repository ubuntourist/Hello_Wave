#!/bin/bash
# Written by Kevin Cole <ubuntourist@hacdc.org> 2018.04.21 (kjc)
#
# Installs prerequistes for Wave Theory synthesizer
#

echo "Install pip, numpy, scipy, sounddevice, PyQt5 and PySide"
echo "for Python 3? (System-wide. root access required.)"
echo
read -p "Press [ENTER] to continue, ^C to quit." goforth

sudo apt install python3                         \
                 python3-pip                     \
                 python3.pyside                  \
                 python3-pyqt5                   \
                 python3-pyqt5.qtmultimedia      \
                 python3-pyqt5.qtopengl          \
                 python3-pyqt5.qtpositioning     \
                 python3-pyqt5.qtquick           \
                 python3-pyqt5.qtsensors         \
                 python3-pyqt5.qtserialport      \
                 python3-pyqt5.qtsql             \
                 python3-pyqt5.qtsvg             \
                 python3-pyqt5.qtwebkit          \
                 python3-pyqt5.qtwebsockets      \
                 python3-pyqt5.qtx11extras       \
                 python3-pyqt5.qtxmlpatterns     \
                 python3-pyqtgraph               \
		 python3-numpy                   \
		 pyqt5-dev-tools                 \
		 python3-scipy
sudo -H pip3 install sounddevice
