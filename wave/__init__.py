#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Default constants
#
#  Copyright 2018 Kevin Cole <kevin.cole@novawebcoop.org> 2018.04.16
#  _______________________________________________________________
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#  _______________________________________________________________
#
#

#__all__ = ["wave", "sine", "sawtooth", "square", "triangle"]

HALF_STEP  = 2 ** (1 / 12)   # 12th root of 2 (~ 1.0594630943592953)
WHOLE_STEP = HALF_STEP ** 2  # HALF_STEP * HALF_STEP

MAJOR     = [WHOLE_STEP,        # Re
             WHOLE_STEP,        # Mi
             HALF_STEP,         # Fa
             WHOLE_STEP,        # Sol
             WHOLE_STEP,        # La
             WHOLE_STEP,        # Ti
             HALF_STEP]         # Do

CHROMATIC = [HALF_STEP] * 11

# Defaults
#
PITCH     =   440          # 440 Hz. a.k.a. "Concert A"
AMPLITUDE =   120          # ??? decibels? [pulled out of my ass]
SECONDS   =    10          # Should this be cycles?
DEPTH     =   100          # Number of iterations
SCALE     = "major"        # or "chromatic"... for the moment
SAMPLES   = 44100          # Samples per seconds - Nyquist
FPS       = 44100          # Frames per second/frameset (bit rate).

from wave import *
