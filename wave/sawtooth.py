#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  sawtooth.py
#
#  Generate sawtooth wave.
#
#  Copyright 2018 Kevin Cole <kevin.cole@novawebcoop.org> 2018.04.16
#  _______________________________________________________________
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#  _______________________________________________________________
#
#

from math import sin, radians
from wave      import *
from wave.wave import *

__appname__    = "Synthesizer"
__module__     = "Sawtooth"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2018"
__agency__     = "HacDC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class Sawtooth(Wave):
    """Add harmonics with amplitudes of 1/harmonic"""

    def __init__(self, depth=DEPTH):
        """y = sin(x)/1 + sin(2x)/2 + sin(3x)/3 + sin(4x)/4 ..."""
        super(Sawtooth, self).__init__()
        harmonic  = 1
        harmonics = 0
        while harmonics < depth:
            for degree in range(360):
                wave = (sin(radians(degree * harmonic)) / harmonic)
                self.wave[degree] += wave
            harmonic  += 1
            harmonics += 1
