#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  SignalGenerator.py
#
#  This an attempt to teach some fun sound stuff, e.g. 12th root of 2,
#  simple rules for sine wave, square wave, triangle wave, sawtooth wave,
#  wave cancelation, phase shifting, etc.  See:
#
#  WikiBooks
#    Sound Synthesis Theory
#      Additive Synthesis
#        Constructing common harmonic waveforms in additive synthesis
#
#  https://en.wikibooks.org/wiki/Sound_Synthesis_Theory/Additive_Synthesis#Constructing_common_harmonic_waveforms_in_additive_synthesis
#
#  Copyright 2018 Kevin Cole <kevin.cole@novawebcoop.org> 2018.04.16
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#


from __future__ import print_function
from six.moves  import input           # use raw_input when I say input
from os.path    import expanduser      # Cross-platform home directory finder

from PyQt5.QtCore    import *
from PyQt5.QtGui     import *
from PyQt5.QtWidgets import *

import sys

import main_dialog  # Constructed from Qt Designer & pyuic5

__appname__    = "Signal Generator"
__module__     = ""
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2018"
__agency__     = "HacDC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class MainDialog(QDialog, main_dialog.Ui_mainDialog):
    def __init__(self, parent=None):
        """Construct a Dialog window and fill with widgets"""
        super(MainDialog, self).__init__(parent)  # Old way. Learn new way.
        self.setupUi(self)


def main():
    # Instantiate a form, show it and start the app.
    QCoreApplication.setApplicationName(__appname__)
    QCoreApplication.setApplicationVersion(__version__)
    QCoreApplication.setOrganizationName("HacDC")
    QCoreApplication.setOrganizationDomain("hacdc.org")

    app = QApplication(sys.argv)
    form = MainDialog()
    form.show()
    app.exec_()


if __name__ == "__main__":
    main()
